#include<iostream>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<conio.h>
using namespace std;
//tarea hecha1
void Menu(int *);


void SistEcu();
void Matrices_1();
void Matrices_2();
void CambioBase();
void ParImpar();

int main(){
    int opc;
    do{
        Menu(&opc);//opc = numero entero 0 1 2 o 3
        switch(opc){
            case 1:
                SistEcu();//Sistema de ecuaciones
                break;
            case 2:
                Matrices_1();//Operacion suma y multiplicacion
                Matrices_2();//Operacion transpuesta y determinante
                break;
            case 3:
                CambioBase();//Cambiar de base
                break;
            case 0:
                cout<<"Saliendo ..."<<endl;
                break;
            default:
                cout<<"Error en la insercion"<<endl;
        }

        system("cls");
    }while(opc!=0);


    return 0;
}

void Menu(int *opc){

    printf("Inserte la operacion que desea realizar(numero)\n");
    printf("  (1)Sistema de ecuaciones\n");
    printf("  (2)Matrices\n");
    printf("  (3)Cambio de base de un numero\n");
    printf("  (0)Salir\n");

    cin>>*opc;
    do{
        if(*opc>3 || *opc<0){
            printf("Valor no permitido(1-3)\n");
            printf("Vuelva a intentar\n");
            cin>>*opc;
        }
    }while(*opc>3 || *opc<0);
}

void SistEcu(){
	int opcion=0,i=0;
	float x1,x2,x3,a,b,c,d,x,r,mult,suma;
	cout<<"\t******************Raices de Ecuaciones******************"<<endl<<endl;
   cout<<"\t---------------------------------------------------"<<endl<<endl;
  cout<<"\t1. Ecuacion Lineal de forma -> a*x + b = 0 "<<endl;
  cout<<"\t2. Ecuacion cuadratica de forma -> a*x^2 + b*x + c "<<endl;
  cout<<"\t3. Ecuacion cubica de forma -> a*x^3 + b*x^2 +c*x +d "<<endl<<endl;  
  cout<<"Ingrese Numero de opcion : ";cin>>opcion;
  
  	while(opcion<1 || opcion>3){
  		cout<<"Opcion no valida, ingresa la opcion de nuevo :";
  		cin>>opcion;
  	}

  cout<<endl<<endl;
  switch(opcion){
  	case 1 :
	  system("cls"); 	
	cout<<endl;
	cout<<"*******Ecuacion Lineal*******";
	cout<<endl<<endl<<endl;
	cout<<"\ta*x + b = 0";
	cout<<endl<<endl<<endl;	
	cout<<"Valor de a :";cin>>a;
  	cout<<"Valor de b :";cin>>b; 
  	x=-b/a;
  	cout<<endl<<endl<<endl;
	cout<<"\t"<<a<<"*x + "<<b<<" = 0 ->"<<" x = "<<x;  
  	break;
  	case 2 :
	  system("cls");  
	cout<<endl;
	cout<<"*******Ecuacion cuadratica*******";
	cout<<endl<<endl<<endl;	
	cout<<"\ta*x^2 + b*x + c = 0 ";
	cout<<endl<<endl<<endl;	
  	cout<<"Valor de a :";cin>>a;
  	cout<<"Valor de b :";cin>>b; 
  	cout<<"Valor de c :";cin>>c; 
  	d=b*b-4*a*c;
  	if(d<0){
  		d=-d;
	r=pow(d,0.5)/(2*a);
	cout<<endl<<endl<<endl;	
  	cout<<"\t"<<a<<"*x^2 + "<<b<<"*x + "<<c<<" = 0 ->"<<" x1 = "<<(-b/(2*a))<<"+"<<r<<"*i"<<endl;  
  	cout<<"                            -> x2 = "<<(-b/(2*a))<<"-"<<r<<"*i";
  	break;
  	}
  	x1=(-b + pow(d,0.5))/(2*a);
  	x2=(-b - pow(d,0.5))/(2*a);
  	cout<<endl<<endl<<endl;
	cout<<"\t"<<a<<"*x^2 + "<<b<<"*x + "<<c<<" = 0 ->"<<" x1 = "<<x1<<endl;  
  	cout<<"                             -> x2 = "<<x2;
	  break;
  	case 3 :
	  system("cls");
	cout<<endl;
	cout<<"*******Ecuacion cubica*******";    
	cout<<endl<<endl<<endl;	
	cout<<"\ta*x^3 + b*x^2 +c*x +d = 0";
  	cout<<endl<<endl<<endl;	
  	cout<<"Valor de a :";cin>>a;
  	cout<<"Valor de b :";cin>>b; 
  	cout<<"Valor de c :";cin>>c;
	cout<<"Valor de d :";cin>>d; 
  	
    x = 1 + rand()%20;
 
	  do{
	  	i++;
	  	x1=x-((a*x*x*x+b*x*x+c*x+d)/(3*a*x*x+2*b*x+c));
	  	x=x1;
	  	
	  }while(i<=1000);
      cout<<endl<<endl<<endl;
	  cout<<"\t"<<a<<"*x^3 + "<<b<<"*x^2 + "<<c<<"*x +"<<d<<" = 0 ->"<<" x1 = "<<x1<<endl;
	  mult=-d/(x1*a);
	  suma=-(x1+b/a);
	  d=suma*suma-4*mult;
  	if(d<0){
  		d=-d;
	r=pow(d,0.5)/2;
  	cout<<"                                   -> x2 = "<<(suma/(2))<<"-"<<r<<"*i"<<endl;
  	cout<<"                                   -> x3 = "<<(suma/(2))<<"+"<<r<<"*i";
  	break;
	  }else
  	x2=(suma + pow(d,0.5))/2;
  	x3=(suma - pow(d,0.5))/2;
	cout<<"                                    -> x2 = "<<x2<<endl;
  	cout<<"                                    -> x3 = "<<x3;	
  	break;
  }
  cout<<endl<<endl<<endl<<endl;
  cout<<"Presione cualquier tecla para volver al menu principal. ";
  getch();
}
void Matrices_1(){
}
void Matrices_2(){
int d;
	printf("Por favor ingrese el orden de la matriz\n");
	cin>>d;

	int matriz[d][d];
	for(int i=0;i<d;i++){
		for(int j=0;j<d;j++){

			cout<<"Ingrese el numero para la posicion ["<<i+1<<"],["<<j+1<<"]:";
			cin>>matriz[i][j];
		}
		printf("\n");
	}

	system("CLS");

	cout<<"La matriz original es:\n";
	for(int i=0;i<d;i++){
		for(int j=0;j<d;j++){

			cout<<matriz[i][j]<<" ";
		}
		printf("\n");
	}

	printf("La matriz transpuesta es:\n");
	for(int i=0;i<d;i++){
		for(int j=0;j<d;j++){

			cout<<matriz[j][i]<<" ";
		}
		printf("\n");
	}

	double aux;
    double determinante=1;
    for(int k=0;k<d-1;k++){//recorrer la diagonal
        determinante*=matriz[k][k];
        if(matriz[k][k]==0){
        }else{

            for(int i=k+1;i<d;i++){//recorrer fila
                aux=-matriz[i][k];
                for(int j=k;j<d;j++){
                    matriz[i][j]=matriz[i][j]+aux*matriz[k][j]/matriz[k][k];
                }

            }
        }

    }
    determinante*=matriz[d-1][d-1];
    cout<<"El determinante es:"<<determinante<<endl;
    cout<<"Presione cualquier tecla para volver al menu";
    getch();

}
void CambioBase(){
unsigned int num,base,total=0,coeficiente=1;

	printf("Este programa va a pasar un numero natural en base 10 a una determinada base.");
	printf(" Introduce el numero: ");
	scanf("%u",&num);
	printf("Introduzca la base a que desea convertir:");
	scanf("%u",&base);
	while(num!=0)
    {
        total=total+coeficiente*(num%base);
        num=num/base;
        coeficiente*=10;
    }
    printf("El numero a base %u es :%u",base,total);
    getch();}
