
package dados;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Dados {

    static int Jugadores(){
        int cant;
        Scanner entrada=new Scanner(System.in);
        System.out.println("¿Con quien desea jugar?");
        System.out.println(" (1)Otro jugador   (2)Computadora   (0)Salir");
        cant=entrada.nextInt();
        do{
            try{
                if(cant!=1 && cant!=2 && cant!=0){
                    System.out.println("Valor fuera del rango..intenta denuevo");
                    cant=entrada.nextInt();
                }
            }catch(InputMismatchException exception){
                System.out.println("Tipo de dato no valido");
            }
            
        }while(cant!=1 && cant!=2 && cant!=0);
        return cant;
    }
    static void tiemEspera(){
        try {
           Thread.sleep(2000);
        } catch (InterruptedException ex) {
            System.out.println("Error en el tiempo de espera");
        }
    }
    static int dado(){
        int dado1;
        dado1=(int)(Math.random()*(6-1+1)+1);
        return dado1;
    }
    static int Jugador1(int acum){
        int dado1;
        Scanner input_tecla=new Scanner(System.in);
        System.out.println("Inserte (si) para tirar los dados ");
        input_tecla.nextLine();
        
        System.out.println("Tirando el dado...");
        tiemEspera();
        dado1=dado();
        System.out.println("Resultado del dado : "+dado1);
        tiemEspera();
        acum=acum+dado1;
        if(acum>9){
            acum=9-(acum%9);
        }
        
        System.out.println("Casilla en la que te encuentras : "+acum);
        return acum;
    }
    ///////////////////////////AGREGAR LOS CASOS DE LOS CASILLEROS/////
    static int Casos(int acum){
        switch(acum){
            case 1:
                imgTablero_2();
                break;
            case 2:
                imgTablero_2();
                break;
            case 3:
                imgTablero_2();
                break;
            case 4:
                imgTablero_2();
                break;
            case 5:
                imgTablero_2();
                break;
            case 6:
                imgTablero_2();
                break;
            case 7:
                imgTablero_2();
                break;
            case 8:
                imgTablero_2();
                break;
            case 9:
                imgTablero_2();
                break;
        }
        return acum;
    }
    
    ///////////////////////////////////////////////////////////////
    static int JugadorIA(int acum){
        int dado1;
        System.out.println("Lanzando dado de la IA ");
        tiemEspera();
        dado1=dado();
        System.out.println("Resultado del dado : "+dado1);
        tiemEspera();
        acum=acum+dado1;
        if(acum>9){
            acum=9-(acum%9);
        }
        
        System.out.println("Casilla en la que te encuentras IA : "+acum);
        return acum;
    }
    /////IMAGEN DEL TABLERO INICIAL, TAMBIEN LAS ALTERNATIVAS DE CASILLAS/////
    static void imgTablero(){
        
    }
    
    ///IMAGEN DEL TABLERO CUANDO AVANZA////
    static void imgTablero_2(){
        
    }
    public static void main(String[] args){
        int cant;
        int dado1=0,dado2=0;
        int cont1=0,cont2=0;
        int turnos=1;
        boolean fin=false;
        
        do{
            imgTablero();
            cant=Jugadores();
            switch(cant){
                case 1:
                    //////////Juego entre 2 jugadores///////
                    while(fin==false){
                        System.out.println("********** Turno n°"+turnos+" **********");
                        System.out.println("Jugador 1");
                        cont1=Jugador1(cont1);
                        System.out.println("***********************");
                        System.out.println("***********************");
                        
                        System.out.println("Jugador 2");
                        cont2=Jugador1(cont2);
                        System.out.println("***********************");
                        System.out.println("***********************");
                        
                        cont1=Casos(cont1);
                        cont2=Casos(cont2);
                        if(cont1==9){
                            fin=true;
                            System.out.println("***********************");
                            System.out.println("**Ganador : Jugador 1**");
                            System.out.println("***********************");
                        }
                        if(cont2==9){
                            fin=true;
                            System.out.println("***********************");
                            System.out.println("**Ganador : Jugador 2**");
                            System.out.println("***********************");
                        }
                        turnos=turnos+1;
                    }
                    
                    break;
                case 2:
                    /////Juego entre jugador y la IA///////
                    while(fin==false){
                        System.out.println("********** Turno n°"+turnos+" **********");
                        System.out.println("Jugador 1");
                        cont1=Jugador1(cont1);
                        System.out.println("***********************");
                        System.out.println("***********************");
                        
                        System.out.println("Jugador IA");
                        cont2=JugadorIA(cont2);
                        System.out.println("***********************");
                        System.out.println("***********************");
                        cont1=Casos(cont1);
                        cont2=Casos(cont2);
                        if(cont1==9){
                            fin=true;
                            System.out.println("***********************");
                            System.out.println("**Ganador : Jugador 1**");
                            System.out.println("***********************");
                        }
                        if(cont2==9){
                            fin=true;
                            System.out.println("***********************");
                            System.out.println("*****Ganador : IA *****");
                            System.out.println("***********************");
                        }
                        turnos=turnos+1;
                    }
                    break;
                default:
                    System.out.println("Saliendo de la aplicacion");
            }
            cont1=0;
            cont2=0;
            fin=false;
            turnos=1;
        }while(cant!=0);
    }
    
}
