
package operaciones_basicas;

import java.util.*;

public class Operaciones_basicas {
    
    static void SistEcu(){
        
    }
    static void Matrices_1(){
    int matriza[][] = new int [3][3];
         int matrizb[][] = new int [3][3];
         int matrizc[][] = new int [3][3];
         int i, j;
         
         Scanner dato = new Scanner (System.in);
         
         System.out.println("Datos de la Matriz A :");
         
         for (i=0; i<=2; i++){
             for (j=0; j<=2; j++){
                 System.out.print("Escribir valor " + i + " , " + j + " : ");
                 matriza [i][j]= dato.nextInt();
                }
            }
            
            System.out.println("Datos de la Matriz B :");
            
            for (i=0; i<=2; i++){
             for (j=0; j<=2; j++){
                 System.out.print("Escribir valor " + i + " , " + j + " : ");
                 matrizb [i][j]= dato.nextInt();
                }
            }
            
            for (i=0; i<=2; i++){
             for (j=0; j<=2; j++){
                 matrizc [i][j]= matriza[i][j]+matrizb[i][j];
                }
            }
            
            System.out.println("Matriz resultante de la suma :");
    for (i=0;i<=2;i++){
            for (j=0;j<=2;j++) {
                System.out.print(matrizc[i][j] + " ");
            }
            System.out.println("");
            
        }
        
    }
 
    static void Matrices_2(){
        Scanner tem = new Scanner(System.in);
	int d;
        
        System.out.println("Por favor ingrese el orden de la matriz: ");
	d = tem.nextInt();
	
	Scanner entrada = new Scanner(System.in);
	int matriz[d][d];
	for(int i=0;i<d;i++){
		for(int j=0;j<d;j++){

			System.out.print("Ingrese el numero para la posicion ["+i+"]["+j+"]:");
			matriz[i][j] = entrada.nextInt();
		}
		System.out.println("");
	}

	system("CLS");///////////////////////////////////////

	System.out.println("La matriz original es:");
	for(int i=0;i<d;i++){
		for(int j=0;j<d;j++){

			System.out.print(matriz[i][j]<<" ";
		}

		System.out.println("");
	}

	System.out.println("La matriz transpuesta es:");
	for(int i=0;i<d;i++){
		for(int j=0;j<d;j++){

			System.out.print(matriz[i][j]<<" ";
		}
		System.out.println("");
	}

	double aux;
    double determinante=1;
    for(int k=0;k<d-1;k++){//recorrer la diagonal
        determinante*=matriz[k][k];
        if(matriz[k][k]==0){
            
        }else{

            for(int i=k+1;i<d;i++){//recorrer fila
                aux=-matriz[i][k];
                for(int j=k;j<d;j++){
                    matriz[i][j]=matriz[i][j]+aux*matriz[k][j]/matriz[k][k];
                }

            }
        }

    }
    determinante*=matriz[d-1][d-1];
    System.out.println("El determinante es:" + determinante);
    System.out.println("Presione cualquier tecla para volver al menu");
    }
    static void CambioBase(){
        Scanner entrada= new Scanner(System.in);
        int num,base,total=0,coeficiente=1;
        System.out.println("Este programa va a pasar un numero natural en base 10 a una determinada base. Introduce el numero y la base: ");
        num=entrada.nextInt();
        base=entrada.nextInt();
        
    while(num!=0)
    {
        total=total+coeficiente*(num%base);
        num=num/base;
        coeficiente*=10;
    }
        System.out.println("El nuevo numero es:"+total);
        
    }
    static int Menu(){
        Scanner entrada=new Scanner(System.in);
        int opc=-1;
        System.out.println("Inserte la operacion a realizar(numero)");
        System.out.println(" (1)Sistema de ecuaciones");
        System.out.println(" (2)Matrices");
        System.out.println(" (3)Cambio de base de un numero");
        System.out.println(" (0)Salir");
        try{
            opc=entrada.nextInt();
            do{
                if(opc>3 || opc<0){
                    System.out.printf("Valor no permitido(1-3)\n");
                    System.out.printf("Vuelva a intentar\n");
                    opc=entrada.nextInt();
                }
            }while(opc>3 || opc<0);
        }catch(InputMismatchException exception){
            System.out.println("Error");
            entrada.nextLine();
        }
        return opc;
        
    }
    
    public static void main(String[] args) {
        int opc;
        do{
            opc=Menu();
            switch(opc){
                case 1:
                    SistEcu();
                    break;
                case 2:
                    Matrices_1();
                    Matrices_2();
                    break;
                case 3:
                    CambioBase();
                    break;
                case 0:
                    System.out.println("Saliendo de la aplicacion");
                    break;
                default:
                    System.out.println("Error..tipo de dato no valido");
            }
            
        }while(opc!=0);
  

    }
    
}
